local function run(opts, db)
    local utils = require('utils')

    local find = utils.find

    local contains_key = utils.contains_key
    local contains_value = utils.contains_value

    local pkgs = { opts.pkg }

    local mainpkg = find(db, opts.pkg)

    for _, subpkg in ipairs(mainpkg.subpackages) do
        pkgs[#pkgs+1] = subpkg
    end

    local function circular(result)

        -- Packages in here will not trigger recursion, this is for when a package
        -- depends on one of its subpackages
        local whitelist = { result.__visitor.pkgname }

        local tmp = {} -- Will be filled with the dependencies, and they will be searched

        if opts.verbose then
            io.stdout:write(("Visiting %s"):format(result.__visitor.pkgname))
            if result.__pkgname ~= result.__visitor.pkgname then
                io.stdout:write((", via subpackage '%s'"):format(result.__pkgname))
            end
            io.stdout:write("\n")
            io.stdout:flush()
        end

        -- Add the package table itself as derived from aports.db, this allows for a more
        -- accurate checking of repeated packages, otherwise we will have to add every
        -- subpackage and compare names in a loop.
        result.__searched[#result.__searched+1] = result.__visitor
        for _, dep in ipairs(result.__visitor.subpackages) do
            whitelist[#whitelist+1] = dep
        end

        for _, dep in pairs(result.__visitor.depends or {}) do
            if contains_value(whitelist, dep) then
                -- We have a package that depends on itself for whatever
                -- reason, so just go to the next iteration of the loop
                -- without adding it to tmp
                goto continue
            end
            if opts.verbose then
                print(("From=%s Found=%s"):format(result.__pkgname, dep))
            end
            tmp[#tmp+1] =
            {
                pkgname = dep,
                deptype = "d"
            }
            if (
                contains_value(result.__blacklist, dep) and
                not contains_value(whitelist, dep)
                ) then
                result.status = 1
                result.tree[#result.tree+1] =
                {
                    pkgname = result.__pkgname,
                    deptype = result.__deptype,
                }
                result.tree[#result.tree+1] =
                {
                    pkgname = dep,
                    deptype = "d"
                }
            end
            ::continue::
        end
        for _, dep in pairs(result.__visitor.makedepends or {}) do
            if contains_value(whitelist, dep) then
                -- We have a package that depends on itself for whatever
                -- reason, so just go to the next iteration of the loop
                -- without adding it to tmp
                goto continue
            end
            if opts.verbose then
                print(("From=%s Found=%s"):format(result.__pkgname, dep))
            end
            tmp[#tmp+1] =
            {
                pkgname = dep,
                deptype = "m"
            }
            if (
                contains_value(result.__blacklist, dep) and
                not contains_value(whitelist, dep)
                ) then
                result.status = 1
                result.tree[#result.tree+1] =
                {
                    pkgname = result.__pkgname,
                    deptype = result.__deptype,
                }
                result.tree[#result.tree+1] =
                {
                    pkgname = dep,
                    deptype = "m"
                }
            end
            ::continue::
        end
        if not contains_key(result.__visitor.options, "!check") then
            for _, dep in pairs(result.__visitor.checkdepends or {}) do
                if contains_value(whitelist, dep) then
                    -- We have a package that depends on itself for whatever
                    -- reason, so just go to the next iteration of the loop
                    -- without adding it to tmp
                    goto continue
                end
                if opts.verbose then
                    print(("From=%s Found=%s"):format(result.__pkgname, dep))
                end
                tmp[#tmp+1] =
                {
                    pkgname = dep,
                    deptype = "c"
                }
                if (
                    contains_value(result.__blacklist, dep) and
                    not contains_value(whitelist, dep)
                    ) then
                    result.status = 1
                    result.tree[#result.tree+1] =
                    {
                        pkgname = result.__pkgname,
                        deptype = result.__deptype,
                    }
                    result.tree[#result.tree+1] =
                    {
                        pkgname = dep,
                        deptype = "c"
                    }
                end
                ::continue::
            end
        end
        if result.status == 0 then
            -- Final array that holds all the packages from 'tmp' that were not
            -- rejected
            local final_array = {}

            -- Check all values of tmp and if any of them isnt on __searched
            -- or if they even have an entry in the database at all, while
            -- improbable it can happen one subpackage has provides=, which
            -- is not expanded to the mainpkg
            for _, i in ipairs(tmp) do
                if result.__db.apks[i.pkgname] == nil then
                    if opts.verbose then
                        print(("Ignoring '%s', unreachable package"):format(i.pkgname))
                    end
                elseif contains_value(result.__searched, result.__db.apks[i.pkgname][1]) then
                    if opts.verbose then
                        print(("Ignoring '%s', already searched"):format(i.pkgname))
                    end
                else
                    final_array[#final_array+1] = i
                end
            end
            -- Replace tmp with the final_array, which holds only values that were not
            -- rejected
            tmp = final_array
            if #tmp ~= 0 then
                local pkgname = result.__pkgname

                -- Add the package to the tree, if it is a dead-end it will be removed
                -- later on, if it is in the path of hitting a recursion then it will
                -- be kept
                if opts.verbose then
                    print(("Adding '%s' to tree"):format(pkgname))
                end
                result.tree[#result.tree+1] =
                {
                    pkgname = pkgname,
                    deptype = result.__deptype
                }

                -- Create a queue with all the packaeges that should be searched
                -- this will be substracted by 1 for each package that does not hit
                -- a recursion, if the package hits a recursion down the tree then
                -- the result.status == 1 will be propagated down the tree.
                -- if the tree results in no recursive dependencies then it will return
                -- 0 and we will remove it from the tree
                local __queue = #tmp

                for _, i in ipairs(tmp) do
                    result.__pkgname = i.pkgname
                    result.__deptype = i.deptype
                    result.__visitor = result.__db.apks[i.pkgname][1]
                    result = circular(result)
                    if result.status == 1 then
                        break
                    else
                        -- If we didn't hit a recursion then subtract the queue by 1
                        -- once we reach 0 this loop will end and we will remove the
                        -- package from the tree as it is a dead-end
                        __queue = __queue - 1
                        if opts.verbose then
                            print(("'%s' need to search: %s dependencies\n"):format(pkgname, __queue))
                        end
                    end
                end
                if __queue == 0 then
                    if opts.verbose then
                        print(("Removing '%s' from tree, it is a dead-end"):format(pkgname))
                    end
                    result.tree[#result.tree] = nil
                end
            end
        end
    return result
end

    local holder =
    {
        status = 0, -- return code of a query, it is 1 if a recursive dep was found
        tree = {}, -- indexed table that can be printed to print the circular dependency

        __searched = {}, -- Indexed table of pkgs that have been searched
        __pkgname = opts.pkg, -- Name of the package that is being queried (or subpackage)
        __deptype = nil, -- Where the package comes from, depends, makedepends or checkdepends
        __visitor = mainpkg, -- Object of the package that will be queried
        __blacklist = pkgs, -- Packages that will trigger circular dependency
        __db = db, -- Object that holds the package database we are searching in
    }

    local result = circular(holder)

    if result.status == 1 then
        do
            local i = 1
            while i < #result.tree do
                if result.tree[i].deptype ~= nil then
                    io.stdout:write(("(%s)"):format(result.tree[i].deptype))
                end
                io.stdout:write(("%s"):format(result.tree[i].pkgname))
                io.stdout:write('->')
                i = i + 1
            end
            if result.tree[i].deptype ~= nil then
                io.stdout:write(("(%s)"):format(result.tree[#result.tree].deptype))
            end
            io.stdout:write(("%s"):format(result.tree[#result.tree].pkgname))
        end
        io.stdout:write("\n")
        io.stdout:flush()
    end

    return result.status
end

local M = {}

function M.main(arg, db)
    local retcode = 0
    local verbose = false

    local pkgs = {}

    do
        local i = 2
        while i <= #arg do
            if arg[i] == "-v" then
                verbose = true
            elseif arg[i] == "-h" then
                io.stdout:write(
                    ("usage: %s circular <PKG...> [options]\n\n"):format(arg[1]),
                    "Check if a package has a recursive dependency by looking into\n",
                    "each dependency of it recursively until it reaches a dead end\n",
                    "or it reaches the package or one of its subpackages.\n\n",
                    "options are:\n",
                    "   -h  show this message\n",
                    "   -v  be verbose\n\n",
                    "Example:\n",
                    "   FOO depends on BAR which depends on BAZ which depends on FOO\n",
                    ("   %s will print: FOO->BAR->BAZ->FOO\n"):format(arg[1])
                )
                io.stdout:flush()
                return 0
            else
                pkgs[#pkgs+1] = arg[i]
            end
            i = i + 1
        end
    end

    for _, i in ipairs(pkgs) do
        retcode = retcode + run({ pkg = i, verbose = verbose }, db)
    end

    return retcode
end

return M
