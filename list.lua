local prio =
{
    main = 0,
    community = 1,
    testing = 2
}

local function have_arch(pkg, db, arch)
    if arch == nil then
        return true
    end
    local n = 0
    for _,v in pairs(db.apks[pkg]) do
        if not v.arch["!"..arch] and (v.arch.all or v.arch.noarch or v.arch[arch]) then
            n = n + 1
        end
    end
    return n > 0
end

local function run(pkg, db, flags)
    local ret = 0

    local utils = require('utils')

    local repo = utils.repo
    local find = utils.find

    local contains_value = utils.contains_value

    local mainpkg = find(db, pkg)
    local mainrepo = repo(pkg)

    local pkgs = { pkg }
    for _, subpkg in ipairs(mainpkg.subpackages) do
        pkgs[#pkgs+1] = subpkg
    end

    -- If we are asking for violations and our repo is 'main' then it means
    -- there can't be any violations because packages from any repos can
    -- depend on 'main'
    if flags.violate and prio[mainrepo] == prio.main then
        return nil
    end

    -- All the packages we have to rebuild
    local revdeps = utils.revdep.run(db, pkgs)

    -- One table for each repo
    local main = {}
    local community = {}
    local testing = {}

    for _, p in ipairs(revdeps) do
        local r = repo(p)
        if r == "main" and flags.main then
            if have_arch(p, db, flags.archfilter) and not contains_value(main, p) then
                main[#main+1] = p
            end
        elseif r == "community" and flags.community then
            if have_arch(p, db, flags.archfilter) and not contains_value(community, p) then
                community[#community+1] = p
            end
        elseif r == "testing" and flags.testing then
            if have_arch(p, db, flags.archfilter) and not contains_value(testing, p) then
                testing[#testing+1] = p
            end
        end
    end

    local final = {}
    local next = next

    if next(main) then
        -- If the user asked us for violations check if the repository of our mainpkg
        -- is higher than 'main', if it is then the mainpkg is in either 'community'
        -- or 'testing', and packages on main cannot depend on it
        if flags.violate and prio[mainrepo] > prio.main then
            ret = ret + 1
            final.main = main
        end
        if not flags.violate then
            final.main = main
        end
    end

    if next(community) then
        -- If the user asked us for violations check if the repository of our mainpkg
        -- is higher than 'community', if it is then mainpkg is in 'testing', and
        -- packages from 'community' cannot depend on it
        if flags.violate and prio[mainrepo] > prio.community then
            ret = ret + 1
            final.community = community
        end
        if not flags.violate then
            final.community = community
        end
    end

    -- Check if the user asked for violations and don't even bother printing
    -- testing as it's packages can depend on packages of any other repos
    if next(testing) and not flags.violate then
        final.testing = testing
    end

    if not next(final) then
        final = nil
    end
    return final, ret
end

local M = {}

local function help(cmd, retcode)
    io.stdout:write(
        ("usage: %s list <PKG...> [options]\n\n"):format(cmd),
        "List all reverse dependencies of an arbitrary number of packages\n",
        "They are printed out in the YAML%1.1 format and are formatted as:\n\n",
        "$PKG:\n",
        "  $REPO:\n",
        "    $DEPN\n    $DEPN+1\n    $DEPN+x\n\n",
        "options are:\n",
	"   --arch=ARCH list only dependencies that are enabled for ARCH\n",
        "   -s  list only dependencies that violate policy\n",
        "   -m  list dependenices in main\n",
        "   -c  list dependnecies in community\n",
        "   -t  list dependencies in testing\n",
        "   -h  show this message\n\n",
        "   If neither -m, -c or -t is passed then dependencies from all repos\n",
        "   will be shown, otherwise it will print only of the ones asked.\n\n",
        "Example:\n",
        "   foo:\n",
        "     main:\n",
        "     - bar\n",
        "     community:\n",
        "     - baz\n",
        "     - daz\n",
        "     testing:\n",
        "     - laz\n"
    )
    io.stdout:flush()
    return retcode
end

function M.main(arg, db)
    -- Return code, default to 0
    local ret = 0

    local flags =
    {
        violate = false,
        main = false,
        community = false,
        testing = false,
    }
    local pkgs = {}

    do
        local i = 2
        while i <= #arg do
            -- If the user passes this switch then we show just packages from repositories
            -- that are violating the repo policy, if the mainpkg is in 'community', then
            -- this will print all packages that depend on it that are in 'main' because
            -- packages in 'main' cannot depend on packages in 'community'
            if arg[i] == "-s" then
                flags.violate = true
            elseif arg[i]:sub(1,7) == "--arch=" then
                flags.archfilter = arg[i]:sub(8)
            elseif arg[i] == "-m" then
                flags.main = true
            elseif arg[i] == "-c" then
                flags.community = true
            elseif arg[i] == "-t" then
                flags.testing = true
            elseif arg[i] == "-h" then
                return help(arg[1], 0)
            else
                if arg[i]:sub(1,1) == "-" then
                    return help(arg[1], 1)
                end
                pkgs[#pkgs+1] = arg[i]
            end
            i = i + 1
        end
    end

    local final = {}

    if not flags.main and not flags.community and not flags.testing then
        flags.main = true
        flags.community = true
        flags.testing = true
    end

    for _, v in ipairs(pkgs) do
        local table, retcode = run(v, db, flags)
        if table ~= nil then
            final[v] = table
        end
        ret = ret + retcode
    end

    if next(final) then
        local yaml = require('lyaml')
        print(yaml.dump({final}))
    end

    return ret
end

return M
