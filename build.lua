local yaml = require('lyaml')
local lfs = require('lfs')

--- Global constant
local baselogdir = "/tmp/abuildLogs"

-- Implementation of os.getenv that supports a default value as fallback
-- avoids possible boilerplate
local function getenv(key, fallback)
    local value = os.getenv(key)
    if value == nil then
        return fallback
	end
    return value
end

local function warn(formatstr, ...)
	io.stderr:write(("\27[33m>>> WARNING:\27[0m %s\n"):format(formatstr:format(...)))
	io.stderr:flush()
end

local function err(formatstr, ...)
	io.stderr:write(("\27[31m>>> ERROR:\27[0m %s\n"):format(formatstr:format(...)))
	io.stderr:flush()
end

local function msg(formatstr, ...)
	io.stdout:write(("\27[32m>>>\27[0m %s\n"):format(formatstr:format(...)))
	io.stdout:flush()
end

-- Taken from /usr/bin/buildrepo from aports-lua
local function build_aport(pkgname, aportsdir, logfile)
	local success, errmsg = lfs.chdir(aportsdir)
	if not success then
		err("%s", errmsg)
		return nil
	end
	local logredirect = ""
	if logfile ~= nil then
		logredirect = ("> '%s/%s' 2>&1"):format(baselogdir, logfile)
	end
	local cmd = ("APORTSDIR='%s' %s -r -m %s"):
		format(aportsdir, getenv("AX_ABUILD", "abuild"), logredirect)
	msg("%s: Building", pkgname)
	success = os.execute(cmd)
	if success then
		msg("%s: Built", pkgname)
		local mvcmd = ("mv '%s/%s' '%s/success/%s'"):
			format(baselogdir, logfile, baselogdir, logfile)
			os.execute(mvcmd)
	else
		err("%s: Failed building", pkgname)
		local mvcmd = ("mv '%s/%s' '%s/failed/%s'"):
			format(baselogdir, logfile, baselogdir, logfile)
			os.execute(mvcmd)
	end
	return success
end

local function logfile(pkg, repo)
	local dir = ("%s/%s/%s"):format(baselogdir, repo, pkg.pkgname)
	if not lfs.attributes(dir) then
		local path = ""
		for n in string.gmatch(dir, "[^/]+") do
			path = path.."/"..n
			lfs.mkdir(path)
		end
	end
	return ("%s/%s-r%s.log"):format(repo, pkg.pkgver, pkg.pkgrel)
end

local function run(pkg, db)
	local utils = require('utils')

	local contains_key = utils.contains_key
	local contains_value = utils.contains_value

	local basename = utils.basename

	local find = utils.find

	local ret = 0

	local pkgs = { pkg }

	-- Access the 'apks' table from the database, then access the database named
	-- after the package we were given
	local mainpkg = find(db, pkg)

	for _, subpkg in ipairs(mainpkg.subpackages) do
		pkgs[#pkgs+1] = subpkg
	end

	-- All the packages we have to rebuild
	local allpkgs = utils.revdep.run(db, pkgs)

	-- Path of all the packages we have to rebuild in proper order
	local allbuilds = utils.builddirs.run(db, allpkgs)

	-- Table of packages we built successfully
	local success = {}

	-- Table of packages we failed to build
	local failed = {}

	-- Table of packages that were skipped because of a dependency failed
	local skipped = {}

	for _, v in ipairs(allbuilds) do
		local p = basename(v)
		local d = basename(v:match("(.*)/"))
		local opkg = db.apks[p][1]

		-- Use [1] since that is the only table for that package
		local logfs = logfile(opkg, d)

		local subpkgs = {}

		-- Store all subpackages of that package in a table
		for _, subpkg in pairs(opkg.subpackages) do
			if subpkg ~= p then
				subpkgs[#subpkgs+1] = subpkg
			end
		end

		allpkgs = subpkgs
		allpkgs[#allpkgs+1] = p

		-- Check if our package's dependencies are present in the failed
		-- table
		for fail, _ in pairs(failed) do
			if contains_value(pkg.depends, fail) then
				warn("%s: Skipped because of previous failure (%s)", p, fail)
				skipped[p] =
				{
					{
						reason = ("'%s' from depends has failed to build"):format(fail)
					}
				}
				goto continue
			end
			if contains_value(pkg.makedepends, fail) then
				warn("%s: Skipped because of previous failure (%s)", p, fail)
				skipped[p] =
				{
					{
						reason = ("'%s' from makedepends has failed to build"):format(fail)
					}
				}
				goto continue
			end
			-- Don't check checkdepends if !check is set on options
			if contains_key(pkg.options, "!check") then
				if contains_value(pkg.checkdepends, fail) then
					warn("%s: Skipped because of previous failure (%s)", p, fail)
					skipped[p] =
					{
						{
							reason = ("'%s' from checkdepends has failed to build"):format(fail)
						}
					}
					goto continue
				end
			end
		end

		-- Try to build the aport, it will return a boolean given from os.execute
		local result = build_aport(p, v, logfs)
		if result then
			success[p] = logfs
		else
			failed[p] = logfs
			ret = ret + 1
		end
	::continue::
	end

	-- Final table
	local final = {}

	-- bind next to a local variable
	local next = next

	if next(success) then
		final.success = success
	end

	if next(skipped) then
		final.skipped = skipped
	end

	if next(failed) then
		final.failed = failed
	end

	print(yaml.dump({final}))

	return ret
end

local M = {}

function M.main(arg, db)
	for _, v in ipairs(arg) do
		if v == "-h" then
			io.stdout:write(
				("usage: %s build <PKG...> [options]\n\n"):format(arg[1]),
				"Take a package and build all packages that depend on it, respecting\n",
				"the build order. Messages will be printed telling which package is\n",
				"being built and if it built, failed or was skipped.\n\n",
				"Packages that fail are checked when building future packages and those\n",
				"are skipped. Example: Package FOO has reverse dependencies BAR and BAZ\n",
				"BAR also depends on BAZ, if BAZ fails to build then BAR will be skipped.\n\n",
				"In the end a YAML%1.1 formatted string will be printed, each key of the\n",
				"table will be one key which is the state (success, skipped, fail), inside\n",
				"each of those keys there is a key for each package and the value of the key\n",
				"is the path to the log. In case of skipped there is no log and the key that\n",
				"represents the package will hold instead a key called 'reason' and with it\n",
				"a string that tells which package caused the skip and which dependency type\n",
				"it was, depend, makedepend or checkdepend.\n\n",
				"options are:\n",
				"	-h	show this message"
			)
			io.stdout:flush()
			return 0
		end
	end
	return run(arg[2], db)
end

return M
