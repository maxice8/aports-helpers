local function run(pkgname, db)
    local utils = require('utils')

    local repo = utils.repo
    local find = utils.find
    local contains_key = utils.contains_key
    local contains_value = utils.contains_value

    -- Nicely handled messages to the user
    local function nodeps(name, from, to)
        io.stderr:write(("Package '%s/%s' has no dependencies in '%s'\n"):
            format(from, name, to))
        io.stderr:flush()
    end

    local function goup(name, from, to)
        io.stderr:write(("Package '%s/%s' has dependencies in '%s'\n"):
            format(from, name, to))
        io.stderr:flush()
    end

    -- Our exit code
    local ret = 0

    -- Find and return the database object of our package, it is used
    -- to track the package itself without relying on the pkgname given
    -- which might refer to a subpackage
    local pkg = find(db, pkgname)

    -- Figure out the repository for the mainpkg, use pkg.pkgname to check
    -- for the name
    local mainrepo = repo(pkg.pkgname)

    -- Table that holds the name of all our packages
    local pkgs = { pkg.pkgname }
    for _, subpkg in ipairs(pkg.subpackages) do
        pkgs[#pkgs+1] = subpkg
    end

    -- All the packages we have to rebuild
    local revdeps = utils.revdep.run(db, pkgs)

    -- Check if the dependencies we have acquired can be ignored by being
    -- in 'checkdepends' when we have '!check' in 'options'
    -- Iterate over every element of the revdeps array, we don't ignore the
    -- index here because we need it to remove the element from the table
    -- if the mainpkg is only in 'checkdepends' and '!check' is in 'options='
    for i, p in ipairs(revdeps) do
        local o = db.apks[p][1]
        if o.checkdepends ~= nil and contains_key(o.options, "!check") then
            local ignore = true

            -- Will hold both 'depends' and 'makedepends' of a package
            local depends = o.depends

            -- Join the 'makedepends' into the 'depends', no worries about duplicates
            for _, dependency in ipairs(o.makedepends) do
                depends[#depends+1] = dependency
            end

            -- For every value in our super-depends check if any matches our package
            -- or any subpackage, this is specially important as most stuff in 'makedepends'
            -- refers to -dev subpackages, not the main package itself
            for _, dependency in ipairs(depends) do
                -- Try to match the name given to us to a database object
                -- this might not always be possible like for example llvm-dev
                -- which is at the time of this comment provided by llvm10-dev
                -- but because of particularities of lua-aports it won't be registered
                local dbdep = find(db, dependency)
                if dbdep ~= nil then
                    if dbdep == pkg then
                        ignore = false
                    end
                else
                    -- We hit here once we hit a special case of a provider= in a subpackage
                    -- like in the case of llvm-dev mentioned above, so check by name it is
                    -- not likely we will hit anything but worth trying
                    if contains_value(pkgs, dependency) then
                        ignore = false
                    end
                end
            end

            if ignore then
                table.remove(revdeps, i)
            end
        end
    end

    -- One table for each repo
    local main = {}
    local community = {}
    local testing = {}

    for _, p in ipairs(revdeps) do
        local r = repo(p)
        if r == "main" then
            main[#main+1] = p
        elseif r == "community" then
            community[#community+1] = p
        elseif r == "testing" then
            testing[#testing+1] = p
        end
    end

    local final = {}
    local next = next

    if next(main) then
        final.main = main
    end

    if next(community) then
        final.community = community
    end

    if next(testing) then
        final.testing = testing
    end

    -- Perform various checks to return a proper exit code
    if mainrepo == "main" then
        -- If our package is in repo main, then check if we don't have any dependencies
        -- that are also on main/, that means we can move it to community
        if not contains_key(final, "main") then
            ret = ret + 1
            nodeps(pkg.pkgname, "main", "main")
            -- Check if we also don't have any packages in community, which means this
            -- package can go straight to testing
            if not contains_key(final, "community") then
                ret = ret + 1
                nodeps(pkg.pkgname, "main", "community")
            end
        end
    elseif mainrepo == "community" then
        -- Check if we don't have any packages in community, this means this package can
        -- go straight to testing
        if not contains_key(final, "community") then
            ret = ret + 1
            nodeps(pkg.pkgname, "community", "community")
        end
        -- Check if we have any packages in main, that means this package should go to main
        if contains_key(final, "main") then
            ret = ret + 1
            goup(pkg.pkgname, "community", "main")
        end
    elseif mainrepo == "testing" then
        -- Check if we have any packages in community, that means this package should go to
        -- community
        if contains_key(final, "community") or contains_key(final, "main") then
            -- Check if we have any packages in main, that means this package should go to main
            if contains_key(final, "main") then
                goup(pkg.pkgname, "testing", "main")
            else
                goup(pkg.pkgname, "testing", "community")
            end
            ret = ret + 1
        end
    end
    -- Return the error code
    return ret
end

local M = {}

function M.main(arg, db)
    -- Set up the database, it is a big push so only do it if we are sure 'arg'
    -- isn't nil
    local retcode = 0

    local pkgs = {}

    do
        local i = 2
        while i <= #arg do
            if arg[i] == "-h" then
                io.stdout:write(
                    ("usage: %s check <PKG...> [options]\n\n"):format(arg[1]),
                    "Take one or more packages and get their reverse dependencies\n",
                    "then check if there are any reverse dependencies that violate\n",
                    "Alpine Linux policy.\n\n",
                    "options are:\n",
                    "   -h  show this message\n\n",
                    "Policies that can be violated:\n",
                    "   - PKG has no DEP in the same repository, it can be moved to a lower one\n",
                    "   - PKG has DEP in upper repository, it should be moved to it\n\n",
                    "Examples:\n",
                    "   - main/FOO has no DEP in main, can be moved to community\n",
                    "   - main/FOO has no DEP in main or community, can be moved to testing\n",
                    "   - community/FOO has no DEP in community, can be moved to testing\n",
                    "   - community/FOO has a DEP in main, should be moved to main\n",
                    "   - testing/FOO has a DEP in community, should be moved to community\n",
                    "   - testing/FOO has a DEP in main, should be moved to main\n"
                )
                io.stdout:flush()
                return 0
            end
            pkgs[#pkgs+1] = arg[i]
            i = i + 1
        end
    end

    for _, i in ipairs(pkgs) do
        retcode = retcode + run(i, db)
    end

    return retcode
end

return M
