local M = {}

local lfs = require('lfs')

local function checkfile(d, r, p)
    return lfs.attributes(d.."/"..r.."/"..p.."/APKBUILD", "mode") == "file"
end

function M.basename(path)
	return path:match("^.+/(.+)$")
end

function M.contains_key(set, key)
	-- check for nil values being given to us
	if type(set) == "nil" then return false end
    return set[key] ~= nil
end

function M.contains_value(set, val)
	-- Check if the user really passed us a table
	-- there is a possibility they can pass us a 'nil'
	if type(set) ~= "table" then return false end
    for _, v in pairs(set) do
        if v == val then
            return true
        end
    end
    return false
end

function M.repo(pkgname)
    local dir = lfs.currentdir()

    if checkfile(dir, "main", pkgname) then
        return "main"
    elseif checkfile(dir, "community", pkgname) then
        return "community"
    elseif checkfile(dir, "testing", pkgname) then
        return "testing"
    end
end

function M.find(db, pkgname)
	local pkg = db.apks[pkgname]
	local byname = false

	if pkg == nil then
		return nil
	end

	if #pkg == 1 then
		pkg = pkg[1]
	else
		for _, p in ipairs(pkg) do
			if p.pkgname == pkgname then
				pkg = p
				byname = true
			end
		end
		if not byname then
			pkg = pkg[1]
		end
	end

	return pkg
end

M.revdep = {
	desc = "Print reverse dependencies",
	usage = "PKG...",
	run = function(db, opts)
		local results = {}
		for i = 1, #opts do
			for pkg in db:recursive_reverse_dependencies(opts[i]) do
				results[#results+1] = pkg
			end
		end
		return results
	end
}

M.builddirs = {
	desc = "Print the build dirs for given packages in build order",
	usage = "PKG...",
	run = function(db, opts)
		local results = {}
		for pkg in db:each_in_build_order(opts) do
			results[#results+1] = pkg.dir
		end
		return results
	end
}

return M
