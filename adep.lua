#!/usr/bin/lua5.2

local lfs = require('lfs')

local function getdir(path)
    local final = ""
    local orig_path = path
    -- We hit this in case we are dealing with a full path
    if lfs.symlinkattributes(path, "target") == nil then
        return path:match("(.*)/").."/?.lua;"..package.path
    end
    while lfs.symlinkattributes(path, "target") ~= nil do
        path = lfs.symlinkattributes(path, "target")
        if path:sub(1, 1) ~= "/" then
            final = orig_path:match("(.*)/").."/"..final
        end
        final = final..path:match("(.*)/")
    end
    return final.."/?.lua;"..package.path
end

-- Add our directory to package.path so we can import the values ourselves
package.path = getdir(arg[0])

-- This hold all the commands that can be used, along with a nice description
-- and usage string
local cmds = {
    build = {
        desc = "Build reverse dependencies of a package",
        usage = "<PKG>   ",
    },
    list = {
        desc = "List reverse dependencies of a package",
        usage = " <PKG...>",
    },
    check = {
        desc = "Check if a package can be moved to a lower repo or must be moved to a higher repo",
        usage = "<PKG...>",
    },
    circular = {
        desc = "Check if a package has a circular dependency",
        usage = "<PKG...>",
    }
}

local function usage(bin)
    io.stdout:write(
        ("usage: %s SUBCOMMAND USAGE [options]\n\n"):format(bin),
        "Subcommands are (run 'SUBCOMMAND -h' for details):\n"
    )
    for k, v in pairs(cmds) do
        io.stdout:write((" %s %s   %s\n"):format(k, v.usage, v.desc))
    end
end

local bin = arg[0]:match("^.+/(.+)$")

if arg[1] == nil or arg[1] == "-h" then
    usage(bin)
    os.exit(0)
end

local cmd = arg[1]
local opts = { bin }
local help = false
do
    local i = 2
    while i <= #arg do
        if arg[i] == "-h" then
            help = true
        end
        opts[#opts+1] = arg[i]
        i = i + 1
    end
end

local status, result = pcall(require, cmd)
if status then
    local db
    if help then
        db = nil
    else
        db = require('aports.db').new(lfs.currentdir(), {"main", "community", "testing"})
    end
    for i, v in ipairs(opts) do
        if i == 1 or v:sub(1, 1) == "-" then
            goto continue
        end
        if db.apks[v] == nil then
            io.stderr:write(("%s does not exist in the apk database\n"):format(v))
            io.stderr:flush()
            table.remove(opts, i)
        end
        ::continue::
    end
    if #opts < 2 then
        os.exit(1)
    end
    os.exit(result.main(opts, db))
else
    io.stderr:write(("'%s' is not a valid command\n"):format(cmd))
    os.exit(1)
end
