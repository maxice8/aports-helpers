# aports-helpers

Collection of helpers written in Lua that use aports-lua

## Installing

There is no Alpine Linux package because these scripts are mostly personal and
subject to change on whatever I desire at the moment.

To install the dependencies run:

```sh
# apk add --virtual .depends-aports-helpers lua5.2 lua5.2-filesystem lua5.2-lyaml lua-aports
```

## License

aports-lua is MIT, parts of the code were copy-pasted the `ap` and `buildrepo` binaries.

So everything is MIT.
